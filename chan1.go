package main

import (
	"fmt"
	"sync"
)

func main() {
	var typed string
	owo := make(chan string, 1)
	var wg sync.WaitGroup
	go printing(owo, &wg)
	fmt.Println("alo")
	fmt.Println("fania lab desu")
	fmt.Println(">.<")
	fmt.Scan(&typed)
	fmt.Println()
	fmt.Println("=====")
	//send data to channel
	owo <- typed
	wg.Wait()
}

func printing(owo <-chan string, wg *sync.WaitGroup) {
	wg.Add(1)
	//waiting data from channel
	fmt.Println(<-owo)
	fmt.Println("======")
	wg.Done()
}
